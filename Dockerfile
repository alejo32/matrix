FROM openjdk:8u191-jre-alpine3.8

RUN apk add curl jq

# Workspace
WORKDIR /usr/share/ripley-automation

# ADD .jar under target from host into this image
ADD target/matrix-docker-bdd.jar matrix-docker-bdd.jar
ADD target/matrix-docker-bdd-tests.jar matrix-docker-bdd-tests.jar
ADD target/test-classes/ test-classes/
ADD target/libs/ libs/
ADD healthcheck.sh healthcheck.sh

ENTRYPOINT sh healthcheck.sh