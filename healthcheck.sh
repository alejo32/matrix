#!/bin/bash

echo "........................................................."
echo "...............R I P L E Y    T E C H ..................."
echo "........................................................."
echo "....C E L U L A  D E   A U T O M A T I Z A C I Ó N ......"
echo "........................................................."
echo "........................................................."
echo "Chequeando si el HUB está listo para ejecutar las pruebas"
echo "IP del hub es -> $HUB_HOST"
echo "........................................................."
echo "........................................................."

while [ "$(curl -s http://$HUB_HOST:4444/status | jq -r .value.nodes[0].availability)" != "UP" ]
do
        sleep 3
done

# start the java command
java -cp matrix-docker-bdd.jar:matrix-docker-bdd-tests.jar:test-classes/:libs/* -DHUB_HOST=$HUB_HOST io.cucumber.core.cli.Main --plugin json:./report/report.json