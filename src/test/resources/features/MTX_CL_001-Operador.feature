@OperadorCL
Feature: Realizamos de manera exitosa el paso 1 de la agenda Asignar Operador Logístico

Background: Realizar login exitoso en web Matrix CL
	Given Accedo a Web Matrix
	And Ingresar Usuario Matrix CL
	And Ingreso password Matrix CL
	And Hago click en Botón Iniciar Sesión
	When Se despliega Dashboard de web Matrix
	And Hago click en opción Flujos de Despacho
	And Hago click en opción Despacho Directo a Cliente
	And cierro menú lateral
	#And Ingreso a crear agenda Productos Ripley

Scenario: Agregar de manera exitosa Operador Logístico
	Given Ingreso nuevo OPL Redex
	And Selecciono opción recomendada
	When Se despliega la información correspondiente al OPL seleccionado
	Then Presiono el Botón Guardar y Continuar de OPL