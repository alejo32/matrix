Feature: Validamos funcionalidad de Menú Lateral en Web Matrix CL

Background: Realizar login exitoso en web Matrix CL
	Given Accedo a Web Matrix
	And Ingresar Usuario Matrix CL
	And Ingreso password Matrix CL
	When Hago click en Botón Iniciar Sesión
	Then Se despliega Dashboard de web Matrix

@MTX_CL_000-Menu @MTX_CL_000-Menu_4.1
Scenario: Ingresar a opción Despacho Directo a Cliente
	Given Hago click en opción Flujos de Despacho
	When Hago click en opción Despacho Directo a Cliente
	Then cierro menú lateral
	
@MTX_CL_000-Menu @MTX_CL_000-Menu_4.2
Scenario: Ingresar a opción Tipos de Servicios general	
	Given Hago click en opción Variables Despacho
	When Hago click en opción Tipos de Servicio
	Then cierro menú lateral de Variables de Despacho
	
@MTX_CL_000-Menu @MTX_CL_000-Menu_4.3
Scenario: Ingresar a opción Tipos de Servicios por Operador Logistico
	Given Hago click en opción Variables Despacho
	When Hago click en opción Tipos de Servicio por OPL
	Then cierro menú lateral de Servicios por Operador Logistico
	
@MTX_CL_000-Menu @MTX_CL_000-Menu_4.4
Scenario: Ingresar a opción Tipos de Servicios por Tarifas de Despacho
	Given Hago click en opción Variables Despacho
	When Hago click en opción Tarifas de Despacho
	Then cierro menú lateral de Tarifas de Despacho
	
@MTX_CL_000-Menu @MTX_CL_000-Menu_4.5
Scenario: Ingresar a opción Agendas para COnfiguracion
	Given Hago click en opción Agendas
	When Hago click en opción Agendas de Despacho
	Then cierro menú lateral de Agendas
	
@MTX_CL_000-Menu @MTX_CL_000-Menu_4.6
Scenario: Ingresar a opción Simulador de Agenda
	Given Hago click en opción Simulador Agenda
	Then cierro menú lateral de Agendas