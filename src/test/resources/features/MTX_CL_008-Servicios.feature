Feature: Como usuario 
quiero validar funcionalidad de Opción Tipos de Servicio

Background: Realizar login exitoso en web Matrix CL
	Given Accedo a Web Matrix
	And Ingresar Usuario Matrix CL
	And Ingreso password Matrix CL
	When Hago click en Botón Iniciar Sesión
	And Se despliega Dashboard de web Matrix
	And Hago click en opción Variables Despacho
	Then Hago click en opción Tipos de Servicio
	And cierro menú lateral de Variables de Despacho

@MTX_CL_008-Servicio @MTX_CL_008-Servicio_8.2
Scenario: Agregar nuevo Servicio
	Given Presiono icono Agregar Nuevo para ver Pantalla Agregar Tipo de Servicio
	When Ingreso Nombre de Servicio "Test QA"
	And Ingreso ocurrencia Maxima "5"
	And Ingreso dias de desface "2"
	And Selecciono Canal de Ventas Todos
	Then Seleccionamos horario de corte "21:00" Lu-Vi
	And Presiono botón Guardar y Volver

@MTX_CL_008-Servicio @MTX_CL_008-Servicio_8.3
Scenario: Editar la Ocurrencia Máxima a 2 correctamente
	Given Presiono icono Editar para ver Pantalla Agregar Tipo de Servicio
	When Modifico la ocurrencia Maxima a "2"
	Then Presiono botón Guardar y Volver
	
@MTX_CL_008-Servicio @MTX_CL_008-Servicio_8.4
Scenario: Editar la Ocurrencia Máxima a 1 correctamente
	Given Presiono icono Editar para ver Pantalla Agregar Tipo de Servicio
	When Modifico la ocurrencia quedando en "1"
	Then Presiono botón Guardar y Volver

@MTX_CL_008-Servicio @MTX_CL_008-Servicio_8.5
Scenario: Editar los días de desfase correctamente
	Given Presiono icono Editar para ver Pantalla Agregar Tipo de Servicio
	When Editar días de desfase
	Then Presiono botón Guardar y Volver

@MTX_CL_008-Servicio @MTX_CL_008-Servicio_8.6
Scenario: Editar los días de desfase aumentando correctamente
	Given Presiono icono Editar para ver Pantalla Agregar Tipo de Servicio
	When Editar días de desfase aumentandolo
	Then Presiono botón Guardar y Volver
	
@MTX_CL_008-Servicio @MTX_CL_008-Servicio_8.7
Scenario: Editar los días de desfase aumentando correctamente
	Given Presiono icono Editar para ver Pantalla Agregar Tipo de Servicio
	When Editar días de desfase disminuyendo
	Then Presiono botón Guardar y Volver

@MTX_CL_008-Servicio @MTX_CL_008-Servicio_8.8
Scenario: Editar el canal de venta TVI, POS con un check o uncheck en los filtros correctamente
	Given Presiono icono Editar para ver Pantalla Agregar Tipo de Servicio
	When Saco ckeck de Canal POS
	Then Presiono botón Guardar y Volver
	
@MTX_CL_008-Servicio @MTX_CL_008-Servicio_8.9
Scenario: Editar Horario de corte a 10:00 am
	Given Presiono icono Editar para ver Pantalla Agregar Tipo de Servicio
	When Modificamos horario de corte "10:00" Lunes
	Then Presiono botón Guardar y Volver
	
	
	
	
	
	

	