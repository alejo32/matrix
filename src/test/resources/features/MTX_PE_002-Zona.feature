@ZonaPE
Feature: Creamos zona de para nueva agenda de despacho

Background: 
	Given Accedo a Web Matrix
	And Ingresar Usuario Matrix PE
	And Ingreso password Matrix PE
	Then Hago click en Botón Iniciar Sesión
	And Se despliega Dashboard de web Matrix
	And  Hago click en opción Flujos de Despacho
	And  Hago click en opción Despacho Directo a Cliente
	Then Se despliega Pantalla Crear Nueva Agenda de Despacho
#	And cierro menú lateral
	Then Ingreso nuevo OPL Trujillo
	And Selecciono opción recomendada
	And Se despliega la información correspondiente al OPL seleccionado
	Then Presiono el Botón Guardar y Continuar


Scenario: Agregar de manera exitosa Operador Logístico
	Given Despliego opción de Nueva Zona
	When Ingreso el Nombre de la Nueva Zona
	And Selecciono Departamento Lima
	And Selecciono todas las Provincias
	And Selecciono todos los Distritos	
	Then Presiono el Botón Guardar y Continuar