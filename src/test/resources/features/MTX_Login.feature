Feature: Validación de Login en Matrix, utiliando Usuarios Válidos y Usuarios erroneos

@MTX_Login @LoginCL @LoginCL2.1
Scenario: Realizar login exitoso en web Matrix CL
	Given Accedo a Web Matrix
	And Ingresar Usuario Matrix CL
	And Ingreso password Matrix CL
	When Hago click en Botón Iniciar Sesión
	Then Se despliega Dashboard de web Matrix
	
@MTX_Login @LoginPE @LoginCL2.1
Scenario: Realizar login exitoso en web Matrix PE
	Given Accedo a Web Matrix
	And Ingresar Usuario Matrix PE
	And Ingreso password Matrix PE
	When Hago click en Botón Iniciar Sesión
	Then Se despliega Dashboard de web Matrix

@MTX_Login @UserFail @LoginCL3.1
Scenario: Ingresar en el login un usuario de correo electronico Ripley invalido
	Given Accedo a Web Matrix
	And Ingresar Usuario inválido
	Then Se despliega mensaje de alerta Correo Ripley no válido
	
@MTX_Login @PassFail @LoginCL3.2
Scenario: Ingresar en el login password invalido
	Given Accedo a Web Matrix
	And Ingresar Usuario Matrix CL
	And Ingreso password inválida
	When Hago click en Botón Iniciar Sesión
	Then Se despliega mensaje de alerta Contraseña no válida