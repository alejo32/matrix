@FlujoPE
Feature: Validamos funcionalidad de Menú Lateral en Web Matrix PE

Background: Realizar login exitoso en web Matrix PE
	Given Accedo a Web Matrix
	And Ingresar Usuario Matrix PE
	And Ingreso password Matrix PE
	When Hago click en Botón Iniciar Sesión
	Then Se despliega Dashboard de web Matrix

Scenario: Ingresar a Dashboard que se encuentra en menú lateral
	Given Hago click en opción Paneles de Control
	Then Se despliega Pantalla Dashboard
	
Scenario: Ingresar a opción Despacho Directo a Cliente
	Given Hago click en opción Flujos de Despacho
	When Hago click en opción Despacho Directo a Cliente
	Then Se despliega Pantalla Crear Nueva Agenda de Despacho
#	And cierro menú lateral