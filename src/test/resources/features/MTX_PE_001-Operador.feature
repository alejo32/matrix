@OepradorPE
Feature: Realizamos de manera exitosa el paso 1 de la agenda Asignar Operador Logístico

Background: Realizar login exitoso en web Matrix PE
	Given Accedo a Web Matrix
	And Ingresar Usuario Matrix PE
	And Ingreso password Matrix PE
	And Hago click en Botón Iniciar Sesión
	When Se despliega Dashboard de web Matrix
	And Hago click en opción Flujos de Despacho
	And Hago click en opción Despacho Directo a Cliente
	Then Se despliega Pantalla Crear Nueva Agenda de Despacho
#	And cierro menú lateral

Scenario: Agregar de manera exitosa Operador Logístico
	Given Ingreso nuevo OPL Trujillo
	And Selecciono opción recomendada
	When Se despliega la información correspondiente al OPL seleccionado
	Then Presiono el Botón Guardar y Continuar