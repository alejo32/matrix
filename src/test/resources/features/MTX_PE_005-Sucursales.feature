@SucursalesPE
Feature: Creamos ruta de acuerdo a Operador Logístico

Background: 
	Given Accedo a Web Matrix
	And Ingresar Usuario Matrix PE
	And Ingreso password Matrix PE
	Then Hago click en Botón Iniciar Sesión
	And Se despliega Dashboard de web Matrix
	And  Hago click en opción Flujos de Despacho
	And  Hago click en opción Despacho Directo a Cliente
	Then Se despliega Pantalla Crear Nueva Agenda de Despacho
#	And cierro menú lateral
	Then Ingreso nuevo OPL Trujillo
	And Selecciono opción recomendada
	And Se despliega la información correspondiente al OPL seleccionado
	Then Presiono el Botón Guardar y Continuar
	And Despliego opción de Nueva Zona
	When Ingreso el Nombre de la Nueva Zona
	And Selecciono Departamento Lima
	And Selecciono todas las Provincias
	And Selecciono todos los Distritos
	Then Presiono el Botón Guardar y Continuar	
	And Despliego opción para agregar Grupo
	Then Realizo click en botón Añadir Grupo
	And Selecciono todas las Provincias del Grupo 1
	And Selecciono todos los distritos
	And Selecciono el formato de plazo como Día
	And Selecciono Frecuencia Lu, Ma y Mi
	And Selecciono el Plazo dejando configurado 3 días
	And Selecciono Volumen Mínimo Chico
	And Selecciono Volumen Máximo Mediano
	Then Presiono el Aceptar
	And Presiono el Botón Guardar y Continuar
	And Despliego Pantalla 4 Creación de Agenda
	When Selecciono Unidad de Medida en M3
	And Despliego información de Agenda
	Then Genero un nombre a la agenda
	And Selecciono el tipo de servicio, como TODAS
	And Ingreso una fecha de inicio
	And Ingreso una fecha de termino
	And Agrego capacidad base por día
	And Seleccionamos no auto generar Agenda
	Then Presiono el Botón Guardar y Volver
	And Presiono el Botón Guardar y Continuar


Scenario: Asociar sucursal de Stock de manera exitosa
	Given Selecciono Sucursal Lima
	When Editamos la Configuración
	And Se despliega pantalla para Asociar Sucursal de Stock
	When Despliego opción de edición de Relación
	And En criterio de Despacho selecciono Canal de Ventas
	And Selecciono Tipos de Servicio
	And En Criterios de Producto selecciono Departamento Marcas Sport 428
	And En seleccionar Línea selecciono Todas
	And Presiono el botón Insertar
	Then Se carga la información correspondiente en la Tabla de registro
	And Presiono el Botón Guardar y Volver
	And Presiono el Botón Guardar y Continuar
	
	
	
	
	