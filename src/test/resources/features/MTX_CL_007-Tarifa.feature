@TarifaCL
Feature: Creamos ruta de acuerdo a Operador Logístico

Background: 
	Given Accedo a Web Matrix
	And Ingresar Usuario Matrix CL
	And Ingreso password Matrix CL
	Then Hago click en Botón Iniciar Sesión
	And Se despliega Dashboard de web Matrix
	And  Hago click en opción Flujos de Despacho
	And  Hago click en opción Despacho Directo a Cliente
	And cierro menú lateral
	And Ingreso a crear agenda Productos Ripley
	Then Ingreso nuevo OPL Redex
	And Selecciono opción recomendada
	And Se despliega la información correspondiente al OPL seleccionado
	Then Presiono el Botón Guardar y Continuar
	And Despliego opción de Nueva Zona
	When Ingreso el Nombre de la Nueva Zona
	And Selecciono la Región Metropolitana
	And Selecciono todas las comunas	
	Then Presiono el Botón Guardar y Continuar
	And Despliego opción para agregar Grupo
	Then Realizo click en botón Añadir Grupo
	And Selecciono todas las comunas del Grupo 1
	And Selecciono el formato de plazo como Día
	And Selecciono Frecuencia Lu, Ma y Mi
	And Selecciono el Plazo dejando configurado 3 días
	And Selecciono Volumen Mínimo Chico
	And Selecciono Volumen Máximo Mediano
	Then Presiono el Aceptar
	And Presiono el Botón Guardar y Continuar
	And Despliego Pantalla 4 Creación de Agenda
	When Selecciono Unidad de Medida en M3
	And Despliego información de Agenda
	Then Genero un nombre a la agenda
	And Selecciono el tipo de servicio, como TODAS
	And Ingreso una fecha de inicio
	And Ingreso una fecha de termino
	And Agrego capacidad base por día
	And Seleccionamos no auto generar Agenda
	Then Presiono el Botón Guardar y Volver
	And Presiono el Botón Guardar y Continuar
	Then Selecciono una o más sucursales
	And Editamos la Configuración
	And Se despliega pantalla para Asociar Sucursal de Stock
	And Despliego opción de edición de Relación
	And En criterio de Despacho selecciono Canal de Ventas
	When Selecciono Tipos de Servicio
	And En Criterios de Producto selecciono Departamento 105
	And En seleccionar Línea selecciono Todas
	And selecciono sublinea de acuerdo a seleccion anterior
	And Ingreso <SKU> a consultar
		|	2000343668569	|
	And Presiono el botón Insertar
	Then Se carga la información correspondiente en la Tabla de registro
	And Presiono el Botón Guardar y Volver
	And Presiono el Botón Guardar y Continuar
	And Presiono el Botón Guardar y Continuar

Scenario: Asignar tarifa de despacho nueva zona
	Given Presiono Boton para editar nueva zona
	And En pantalla Asigno tarifa a Grupo Uno
#	When Selecciono todas las comunas
	And Selecciono Tipo de Servicio
	And Selecciono Tamaño 
	And Selecciono el Criterio de despacho
	And Ingreso <SKU> valido para el Grupo
		|	2000343668569	|
#	Then Presiono el boton Guardar y Volver
#	And Presiono el boton Guardar y Continuar
	
	
	
	
	
	
	
	