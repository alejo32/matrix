Feature: Creamos zona de para nueva agenda de despacho

Background: 
	Given Accedo a Web Matrix
	And Ingresar Usuario Matrix CL
	And Ingreso password Matrix CL
	Then Hago click en Botón Iniciar Sesión
	And Se despliega Dashboard de web Matrix
	And  Hago click en opción Flujos de Despacho
	And  Hago click en opción Despacho Directo a Cliente
	#Then Se despliega Pantalla Crear Nueva Agenda de Despacho
	And cierro menú lateral
	#And Ingreso a crear agenda Productos Ripley
	Then Ingreso nuevo OPL Redex
	And Selecciono opción recomendada
	And Se despliega la información correspondiente al OPL seleccionado
	Then Presiono el Botón Guardar y Continuar de OPL

@MTX_CL_002-Zona @MTX_CL_002-Zona4.2 
Scenario: Agregar de manera exitosa Operador Logístico
	Given Despliego opción de Nueva Zona
	When Ingreso el Nombre de la Nueva Zona
	And Selecciono la Región Metropolitana
	And Selecciono todas las comunas
	Then Presiono el Botón Guardar y Continuar de Zona