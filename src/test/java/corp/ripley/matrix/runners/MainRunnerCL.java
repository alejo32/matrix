package corp.ripley.matrix.runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
		features = "src/test/resources/features",
		glue = {"corp/ripley/matrix/stepdefinitions"},
		tags = "@MTX_CL_000-Menu_4.1",
		plugin = {
				"pretty",
				"html:target/cucumber-reports/cucumber-pretty",
				"json:target/cucumber.json",
				"rerun:target/cucumber-reports/rerun-reports/rerun.txt"
		})
public class MainRunnerCL extends AbstractTestNGCucumberTests {
	/*@Override
	@DataProvider(parallel = true)
	public Object[][] scenarios() {
		return super.scenarios();
	}*/
}