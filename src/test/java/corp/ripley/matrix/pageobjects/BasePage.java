package corp.ripley.matrix.pageobjects;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import corp.ripley.matrix.utils.DriverFactory;
import org.testng.Assert;

public class BasePage extends DriverFactory {
	protected WebDriverWait wait;
	protected JavascriptExecutor jsExecutor;
	//static ExtentTest test;

	public BasePage() throws IOException {
		this.wait = new WebDriverWait(driver, 15);
		jsExecutor = ((JavascriptExecutor) driver);
	}
	
	/* Método genera pausa en el proceso de ejecución */
	public static void waitSleep(int second) {
		long millis = second * 1000;
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/* Método que realiza la captura de pantalla */
	public static void takeScreenShot() throws IOException  {
		DateFormat hora = new SimpleDateFormat("dd-MM-yyyy HHmmss"); 
		Date date = new Date();
		String nombrearchivo = "imagen_"+hora.format(date)+".png";
		String ruta = "./Screenshot/";
		
		File imagen = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(imagen, new File(ruta + nombrearchivo));
	}
	
	/* Método que coloca imagen de la ejecución */
	public static void ultimoScreen() throws IOException{
		File carpeta = new File("./Screenshot/");
		String[] listado = carpeta.list();
		String nomArchivo = null;
		if (listado == null || listado.length == 0) {
			System.out.println("No hay elementos dentro de la carpeta actual");
			return;
		} else {
			for (int i=0; i< listado.length; i++) {					
				nomArchivo = listado[i++];
			}
			//test.addScreenCaptureFromPath("./Screenshot/"+nomArchivo);
		}		
	}
	
	/* Métodos que ejecutan CLICK */
	public void waitAndClickElement(WebElement element) throws InterruptedException {
		boolean clicked = false;
		int attempts = 0;
		while (!clicked && attempts < 10) {
			try {
				this.wait.until(ExpectedConditions.elementToBeClickable(element)).click();
				clicked = true;
			} catch (Exception e) {
				Assert.fail("No es posible clickear en elemento uasndo locator, element: " + "<" + element.toString() + ">");
			}
			attempts++;
		}
	}

	public void waitAndClickElementsUsingByLocator(By by) throws InterruptedException {
		boolean clicked = false;
		int attempts = 0;
		while (!clicked && attempts < 10) {
			try {
				this.wait.until(ExpectedConditions.elementToBeClickable(by)).click();
				clicked = true;
			} catch (Exception e) {
				Assert.fail("No es posible clickear en elemento usando locator, element: " + "<"+ by.toString() + ">");
			}
			attempts++;
		}
	}

	public void clickOnTextFromDropdownList(WebElement list, String textToSearchFor) throws Exception {
		Wait<WebDriver> tempWait = new WebDriverWait(driver, 30);
		try {
			tempWait.until(ExpectedConditions.elementToBeClickable(list)).click();
			list.sendKeys(textToSearchFor);
			Thread.sleep(2000);
			list.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			Assert.fail("No es posible seleccionar la opción solicitada, Exception: " + e.getMessage());
		}
	}


	public void clickOnElementUsingCustomTimeout(WebElement locator, WebDriver driver, int timeout) {
		try {
			final WebDriverWait customWait = new WebDriverWait(driver, timeout);
			customWait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(locator)));
			locator.click();
		} catch (Exception e) {
			Assert.fail("No es posible realizar click en WebElement, Exception: " + e.getMessage());
		}
	}
	
	/* Métodos que ejecutan una ACCION */
	public void actionMoveAndClick(WebElement element) throws Exception {
		Actions ob = new Actions(driver);
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(element)).isEnabled();
			ob.moveToElement(element).click().build().perform();
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement elementToClick = element;
			Boolean elementPresent = wait.until(ExpectedConditions.elementToBeClickable(elementToClick)).isEnabled();
			if (elementPresent == true) {
				ob.moveToElement(elementToClick).click().build().perform();
			}
		} catch (Exception e) {
			Assert.fail("No es posible realizar Action Move y Click en WebElement, Exception: " + e.getMessage());
		}
	}

	public void actionMoveAndClickByLocator(By element) throws Exception {
		Actions ob = new Actions(driver);
		try {
			Boolean elementPresent = wait.until(ExpectedConditions.elementToBeClickable(element)).isEnabled();
			if (elementPresent == true) {
				WebElement elementToClick = driver.findElement(element);
				ob.moveToElement(elementToClick).click().build().perform();
			}
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement elementToClick = driver.findElement(element);
			ob.moveToElement(elementToClick).click().build().perform();
		} catch (Exception e) {
			Assert.fail("No es posible realizar Action Move y Click en WebElement usando locator, Exception: " + e.getMessage());
		}
	}

	/* Métodos Sendkeys */
	public void sendKeysToWebElement(WebElement element, String textToSend) throws Exception {
		try {
			this.WaitUntilWebElementIsVisible(element);
			element.clear();
			element.sendKeys(textToSend);
		} catch (Exception e) {
			Assert.fail("No es posible enviar keys a WebElement, Exception: " + e.getMessage());
		}
	}

	/* Métodos JS y JS SCROLL */
	public void scrollToElementByWebElementLocator(WebElement element) {
		try {
			this.wait.until(ExpectedConditions.visibilityOf(element)).isEnabled();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
			((JavascriptExecutor) driver).executeScript("window.scrollBy(0, -400)");
		} catch (Exception e) {
			Assert.fail("No es posible realizar scroll a WebElement, Exception: " + e.getMessage());
		}
	}

	public void jsPageScroll(int numb1, int numb2) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("scroll(" + numb1 + "," + numb2 + ")");
		} catch (Exception e) {
			Assert.fail("No es posible realizar scroll a WebElement, Exception: " + e.getMessage());
		}
	}

	public void jsClick(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", element);
	}

	/* Metodos de espera para elementos cuando se encuentren visibles */
	public boolean WaitUntilWebElementIsVisible(WebElement element) {
		try {
			this.wait.until(ExpectedConditions.visibilityOf(element));
			return true;
		} catch (Exception e) {
			Assert.fail("WebElement no esta visible, Exception: " + e.getMessage());
			return false;
		}
	}

	public boolean WaitUntilWebElementIsVisibleUsingByLocator(By element) {
		try {
			this.wait.until(ExpectedConditions.visibilityOfElementLocated(element));
			return true;
		} catch (Exception e) {
			Assert.fail("WebElement no esta visible, Exception: " + e.getMessage());
			return false;
		}
	}

	public boolean isElementClickable(WebElement element) {
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(element));
			return true;
		} catch (Exception e) {
			return false;
		}
	}


	public boolean waitUntilPreLoadElementDissapears(By element) {
		return this.wait.until(ExpectedConditions.invisibilityOfElementLocated(element));
	}

	/* Métodos para URL y Páginas */
	public BasePage loadUrl(String url) throws Exception {
		driver.get(url);
		return new BasePage();
	}


	public String getCurrentURL() {
		try {
			String url = driver.getCurrentUrl();
			return url;
		} catch (Exception e) {
			return e.getMessage();
		}
	}
}
