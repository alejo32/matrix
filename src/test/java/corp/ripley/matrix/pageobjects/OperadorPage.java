package corp.ripley.matrix.pageobjects;

import java.io.IOException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OperadorPage extends BasePage{
	
	public @FindBy(xpath = "//*[@class=\"MuiInputBase-input MuiOutlinedInput-input MuiAutocomplete-input MuiAutocomplete-inputFocused MuiInputBase-inputAdornedEnd MuiOutlinedInput-inputAdornedEnd MuiInputBase-inputMarginDense MuiOutlinedInput-inputMarginDense\"]") WebElement txt_OPL;
	public @FindBy(xpath = "//*[@class=\"MuiInputBase-input MuiOutlinedInput-input MuiAutocomplete-input MuiAutocomplete-inputFocused MuiInputBase-inputAdornedEnd MuiOutlinedInput-inputAdornedEnd MuiInputBase-inputMarginDense MuiOutlinedInput-inputMarginDense\"]") WebElement txt_OPLPE;
	public @FindBy(xpath = "//*[@class=\"MuiButton-label\"]") WebElement btn_Guardar;
	public @FindBy(xpath = "//*[@id=\'root\']/div/main/div[2]/div/div/div/div[3]/div/div[2]/div[2]") WebElement divScroll;
	
	public OperadorPage() throws IOException {
		super();
	}

	public OperadorPage txtOPL() throws IOException {
		txt_OPL.sendKeys("10095");
	return new OperadorPage();
	}
	
	public OperadorPage txtOPLPE() throws IOException {
		txt_OPLPE.sendKeys("20040");
	return new OperadorPage();
	}
	
	public OperadorPage txtOPLSel() throws IOException {
		txt_OPL.sendKeys(Keys.ARROW_DOWN);
		txt_OPL.sendKeys(Keys.ENTER);
	return new OperadorPage();
	}
	
	public OperadorPage txtOPLSelPE() throws IOException {
		txt_OPLPE.sendKeys(Keys.ARROW_DOWN);
		txt_OPLPE.sendKeys(Keys.ENTER);
	return new OperadorPage();
	}
	
	public OperadorPage scrollOPL() throws IOException {
		scrollToElementByWebElementLocator(divScroll);
	return new OperadorPage();
	}
	
	public OperadorPage hagoClickEnBotonGuardarDeOPL() throws IOException {
		btn_Guardar.click();
		return new OperadorPage();
	}
}
