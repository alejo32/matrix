package corp.ripley.matrix.pageobjects;

import java.io.IOException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ZonaPage extends BasePage {
	
	public @FindBy(xpath = "//*[@id=\'panel1a-header\']") WebElement exp_NuevaZona;
	public @FindBy(xpath = "//*[@id=\'panel1a-content\']/div/div/div[1]/form/div[1]/div/div/div/input") WebElement txt_Zona;
	public @FindBy(xpath = "//*[@id=\'Seleccionar región\']") WebElement sel_Region;
	public @FindBy(xpath = "//*[@id=\'menu-\']/div[3]/ul/li[1]") WebElement sel_Metropolitana;
	public @FindBy(xpath = "//*[@id=\'mui-component-select-comunas\']") WebElement sel_Comunas;
	public @FindBy(xpath = "//*[@id=\'menu-comunas\']/div[3]/ul/li[1]/span[1]/span[1]/input") WebElement sel_Todas;
	public @FindBy(xpath = "//*[@id=\'Seleccionar departamento\']") WebElement sel_Depto;
	public @FindBy(xpath = "//*[@id=\'menu-\']/div[3]/ul/li[11]") WebElement sel_Dep_Lima;
	public @FindBy(xpath = "//*[@id=\'mui-component-select-provincias\']") WebElement sel_Prov;
	public @FindBy(xpath = "//*[@id=\'menu-provincias\']/div[3]/ul/li[1]/span[1]/span[1]/input") WebElement sel_ProvTodas;
	public @FindBy(xpath = "//*[@id=\'mui-component-select-distritos\']") WebElement sel_Dist;
	public @FindBy(xpath = "//*[@id=\'menu-distritos\']/div[3]/ul/li[1]/span[1]/span[1]/input") WebElement sel_DistTodos;
	public @FindBy(xpath = "//*[@class=\"MuiButtonBase-root MuiButton-root MuiButton-text jss723 MuiButton-textPrimary\"]") WebElement btnGuardarYContinuarZona;

	public ZonaPage() throws IOException {
		super();
	}
	
	public ZonaPage nuevaZona() throws IOException {
		exp_NuevaZona.click();
		return new ZonaPage();
	}
	
	public ZonaPage txtZona() throws IOException {
		for(int i=0; i<=13; i++) {	    	
			txt_Zona.sendKeys(Keys.BACK_SPACE);
	    }
		txt_Zona.sendKeys("Test Automatizado");
		return new ZonaPage();
	}
	
	public ZonaPage selRegion() throws IOException {
		sel_Region.click();
		return new ZonaPage();
	}
	
	public ZonaPage selMetrop() throws IOException {
		sel_Metropolitana.click();
		return new ZonaPage();
	}
	
	public ZonaPage selComunas() throws IOException {
		sel_Comunas.click();
		return new ZonaPage();
	}
	
	public ZonaPage selTodas() throws IOException {
		sel_Todas.click();
		return new ZonaPage();
	}
	
	public ZonaPage selDepto() throws IOException {
		sel_Depto.click();
		return new ZonaPage();
	}
	
	public ZonaPage selDeptoLima() throws IOException {
		sel_Dep_Lima.click();
		return new ZonaPage();
	}
	
	public ZonaPage selProvi() throws IOException {
		sel_Prov.click();
		return new ZonaPage();
	}
	
	public ZonaPage selProviTodas() throws IOException {
		sel_ProvTodas.click();
		return new ZonaPage();
	}
	
	public ZonaPage selDist() throws IOException {
		sel_Dist.click();
		return new ZonaPage();
	}
	
	public ZonaPage selDistTodas() throws IOException {
		sel_DistTodos.click();
		return new ZonaPage();
	}

	public ZonaPage presionoBotonGuardarYContinuarEnZona() throws IOException {
		btnGuardarYContinuarZona.click();
		return new ZonaPage();
	}


}
