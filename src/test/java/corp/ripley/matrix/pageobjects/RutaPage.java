package corp.ripley.matrix.pageobjects;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RutaPage extends BasePage {
	
	public @FindBy(xpath = "//*[@id=\'panel1a-header\']") WebElement exp_Grupo;
	public @FindBy(xpath = "//*[@id=\'panel1a-content\']/div/div/div[2]/div/div[2]") WebElement btn_AddGrupo;
	public @FindBy(xpath = "//*[@id=\'mui-component-select-Comunas\']") WebElement sel_Comunas;
	public @FindBy(xpath = "//*[@id=\'menu-Comunas\']/div[3]/ul/li[1]/span[1]/span[1]/input") WebElement sel_Todos;
	public @FindBy(xpath = "//*[@id=\'Formato de plazo Ultima milla\']") WebElement sel_Formato;
	public @FindBy(xpath = "//*[@id=\'menu-\']/div[3]/ul/li[1]") WebElement sel_Dia;
	public @FindBy(xpath = "//*[@class=\"MuiButtonBase-root MuiButton-root MuiButton-text jss1029 MuiButton-textSizeSmall MuiButton-sizeSmall\"]") WebElement add_Dia;
	public @FindBy(xpath = "//input[@class=\"jss1067\"]") List<WebElement> selectDays;
	public @FindBy(xpath = "//*[@id=\'Minimo\']") WebElement sel_Min;
	public @FindBy(xpath = "//*[@id=\'menu-\']/div[3]/ul/li[3]") WebElement sel_Small;
	public @FindBy(xpath = "//*[@id=\'Maximo\']") WebElement sel_Max;
	public @FindBy(xpath = "//*[@id=\'menu-\']/div[3]/ul/li[4]") WebElement sel_Middle;
	public @FindBy(xpath = "//*[@class=\"MuiButtonBase-root MuiButton-root MuiButton-text jss723 MuiButton-textSizeSmall MuiButton-sizeSmall\"]") WebElement btn_Aceptar;
	public @FindBy(xpath = "//*[@id=\'mui-component-select-Provincias\']") WebElement sel_Prov;
	public @FindBy(xpath = "//*[@id=\'menu-Provincias\']/div[3]/ul/li[1]/span[1]/span[1]/input") WebElement sel_ProvTodas;
	public @FindBy(xpath = "//*[@id=\'mui-component-select-Distritos\']") WebElement sel_Dist;
	public @FindBy(xpath = "//*[@id=\'menu-Distritos\']/div[3]/ul/li[1]/span[1]/span[1]/input") WebElement sel_DistTodos;
	public @FindBy(xpath = "//*[@class=\"MuiButtonBase-root MuiButton-root MuiButton-text jss723 MuiButton-textPrimary\"]") WebElement btnGuardarYContinuarDeRuta;
	
	
	

	public RutaPage() throws IOException {
		super();
	}
	
	public RutaPage nuevoGrupo() throws IOException {
		exp_Grupo.click();
	return new RutaPage();
	}
	
	public RutaPage addGrupo() throws IOException {
		btn_AddGrupo.click();
	return new RutaPage();
	}
	
	public RutaPage selComunas() throws IOException {
		sel_Comunas.click();	
	return new RutaPage();
	}
	
	public RutaPage selTodas() throws IOException {
		sel_Todos.click();	
	return new RutaPage();
	}
	
	public RutaPage selFormato() throws IOException {
		sel_Formato.click();	
	return new RutaPage();
	}
	
	public RutaPage selDia() throws IOException {
		sel_Dia.click();	
	return new RutaPage();
	}
	
	public RutaPage addDia() throws IOException {
		for(int i=0; i<=2; i++) {
			add_Dia.click();
	    }	
	return new RutaPage();
	}
	
	public RutaPage seleccionoLunesMartesYMiercoles() throws IOException {
		selectDays.get(0).click(); // Lunes
		BasePage.waitSleep(2);
		selectDays.get(1).click(); // Martes
		BasePage.waitSleep(2);
		selectDays.get(2).click(); // Miércoles
		BasePage.waitSleep(2);
		return new RutaPage();
	}

	public RutaPage seleccionoLunesyDomingo() throws IOException {
		selectDays.get(0).click(); // Lunes
		BasePage.waitSleep(2);
		selectDays.get(6).click(); // Domingo
		BasePage.waitSleep(2);
		return new RutaPage();
	}
	
	public RutaPage selVolMin() throws IOException {
		sel_Min.click();
		sel_Small.click();
		return new RutaPage();
	}
	
	public RutaPage selVolMax() throws IOException {
		sel_Max.click();
		sel_Middle.click();
		return new RutaPage();
	}
	
	public RutaPage btnAceptar() throws IOException, InterruptedException {
		btn_Aceptar.click();
		return new RutaPage();
	}
	
	public RutaPage selProv() throws IOException {
		sel_Prov.click();
		return new RutaPage();
	}
	
	public RutaPage selProvTodas() throws IOException {
		sel_ProvTodas.click();
		return new RutaPage();
	}
	
	public RutaPage selDist() throws IOException {
		sel_Dist.click();
		return new RutaPage();
	}
	
	public RutaPage selDistTodas() throws IOException {
		sel_DistTodos.click();
		return new RutaPage();
	}

	public RutaPage presionoBotonGuardarYContinuarDeRuta() throws IOException {
		btnGuardarYContinuarDeRuta.click();
		return new RutaPage();
	}

}
	