package corp.ripley.matrix.pageobjects;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

//import cucumber.api.DataTable;


public class SucursalesPage extends BasePage {	
	
	public @FindBy(xpath = "//input[@id=\"mui-92210\"]") WebElement txt_Sucursal;
	public @FindBy(xpath = "//*[@id=\'panel1a-header\']") WebElement exp_Relacion;
	public @FindBy(xpath = "//*[@id=\'mui-component-select-canal de venta\']") WebElement sel_Canal;
	public @FindBy(xpath = "//*[@id=\'menu-canal de venta\']/div[3]/ul/li[1]/span[1]/span[1]/input") WebElement sel_CanalTodo;
	public @FindBy(xpath = "//*[@id=\'mui-component-select-tipos de servicio\']") WebElement sel_TipoSRC;
	public @FindBy(xpath = "//*[@id=\'menu-tipos de servicio\']/div[3]/ul/li[1]/span[1]/span[1]/input") WebElement sel_ServTodo;
	public @FindBy(xpath = "//*[@id=\'full-width-tab-1\']") WebElement menu_Prod;
	public @FindBy(xpath = "//*[@id=\'mui-component-select-departamento\']") WebElement sel_Depto;
	public @FindBy(xpath = "//*[@id=\'menu-departamento\']/div[3]/ul/li[3]/span[1]/span[1]/input") WebElement inp_Depto105;
	public @FindBy(xpath = "//*[@id=\'menu-departamento\']/div[3]/ul/li[5]/span[1]/span[1]/input") WebElement inp_Depto428;
	public @FindBy(xpath = "//*[@id=\'menu-departamento\']/div[3]/ul/li[78]/div") WebElement inp_Depto179;
	public @FindBy(xpath = "//*[@id=\'mui-component-select-linea(s)\']") WebElement sel_Linea;
	public @FindBy(xpath = "//*[@id=\'menu-linea(s)\']/div[3]/ul/li[1]/span[1]/span[1]/input") WebElement sel_LineaTodas;
	public @FindBy(xpath = "//*[@id=\'menu-linea(s)\']/div[3]/ul/li[11]/div") WebElement sel_Linea502800;
	public @FindBy(id = "mui-component-select-sub linea(s)") WebElement sel_Sublinea;
	public @FindBy(xpath = "//*[@id=\'menu-sub linea(s)\']/div[3]/ul/li[14]/div") WebElement sel_Sublinea523115;
	public @FindBy(id = "downshift-2-input") WebElement inp_SKU;
	public @FindBy(xpath = "//*[@id=\"full-width-tabpanel-1\"]/div/div[1]/div[5]/button") WebElement btn_Insertar;
	public @FindBy(xpath = "//*[@id=\'menu-departamento\']/div[1]") WebElement div_Foco;
	public @FindBy(xpath = "//*[@id=\"menu-linea(s)\"]/div[1]") WebElement div_FocoLinea;
	public @FindBy(xpath = "//*[@id=\'menu-sub linea(s)\']/div[1]") WebElement div_FocoSub;
	public @FindBy(xpath = "") WebElement div_FocoInsert;
	public @FindBy(xpath = "/html/body/div[4]/div[3]/div/div/div[1]/div/div[3]/div") WebElement div_Scroll;
	

	public SucursalesPage() throws IOException {
		super();
	}
	
	public SucursalesPage txtSucursal() throws IOException {
		txt_Sucursal.sendKeys("10095");
		return new SucursalesPage();
	}
	
	public SucursalesPage txtSucursalSel() throws IOException {
		txt_Sucursal.sendKeys(Keys.ARROW_DOWN);
		txt_Sucursal.sendKeys(Keys.ENTER);
		return new SucursalesPage();
	}
	
	public SucursalesPage btnEditar() throws IOException {
		Actions action = new Actions(driver);
		WebElement btnEditar = driver.findElement(By.xpath("//button[@class='MuiButtonBase-root MuiFab-root MuiSpeedDial-fab jss1499 MuiFab-primary']"));
		BasePage.waitSleep(2);
		action.moveToElement(btnEditar).moveToElement(driver.findElement(By.xpath("//*[@class='MuiButtonBase-root MuiFab-root MuiSpeedDialAction-fab jss1501 MuiSpeedDialAction-fabClosed MuiFab-sizeSmall' and @title=\"Editar\"]"))).click().build().perform();
		return new SucursalesPage();
	}
	
	public SucursalesPage expRelacion() throws IOException {
		exp_Relacion.click();
		return new SucursalesPage();
	}
	
	public SucursalesPage selCanal() throws IOException {
		sel_Canal.click();
		return new SucursalesPage();
	}
	
	public SucursalesPage selCanalTodo() throws IOException {
		sel_CanalTodo.click();
		return new SucursalesPage();
	}
	
	public SucursalesPage selTipoSRC() throws IOException {
		sel_TipoSRC.click();
		return new SucursalesPage();
	}
	
	public SucursalesPage selServTodo() throws IOException {
		sel_ServTodo.click();
		return new SucursalesPage();
	}
	
	public SucursalesPage menuProd() throws IOException {
		menu_Prod.click();
		return new SucursalesPage();
	}
	
	public SucursalesPage selDepto() throws IOException {
		sel_Depto.click();
		return new SucursalesPage();
	}
	
	public SucursalesPage selDepto179() throws IOException {
		inp_Depto179.click();
		return new SucursalesPage();
	}
	
	public SucursalesPage selLinea() throws IOException {
		sel_Linea.click();
		return new SucursalesPage();
	}
	
	public SucursalesPage selLineaTodos() throws IOException {
		sel_Linea502800.click();
		return new SucursalesPage();
	}
	
	public void selSubLinea() {
		sel_Sublinea.click();
	}
	
	public void selSublinea523115() {
		sel_Sublinea523115.click();
	}
	
	/*public void inpSKU(DataTable dataTable, int row, int column) throws Exception {
		List<List<String>> data = dataTable.raw();
		sendKeysToWebElement(inp_SKU, data.get(row).get(column));
		BasePage.waitSleep(2);
		inp_SKU.sendKeys(Keys.ARROW_DOWN);
		BasePage.waitSleep(1);
		inp_SKU.sendKeys(Keys.ENTER);
	}*/
	
	public SucursalesPage btnInsertar() throws IOException {
		btn_Insertar.click();
		return new SucursalesPage();
	}
	
	public SucursalesPage scrollTabla() throws IOException {
		scrollToElementByWebElementLocator(div_Scroll);
		return new SucursalesPage();
	}
	
	public SucursalesPage divFoco() throws IOException {
		div_Foco.click();
		return new SucursalesPage();
	}
	
	public void divFocoLinea() {
		div_FocoLinea.click();
	}
	
	public void divFocoSub() {
		div_FocoSub.click();
	}
	
	public SucursalesPage txtSucursalPE() throws IOException {
		txt_Sucursal.sendKeys("20066");
		BasePage.waitSleep(2);
		return new SucursalesPage();
	}
	
	public SucursalesPage txtSucursalSelPE() throws IOException {
		txt_Sucursal.sendKeys(Keys.ARROW_DOWN);
		txt_Sucursal.sendKeys(Keys.ENTER);
		return new SucursalesPage();
	}
	
	public SucursalesPage selDepto428() throws IOException {
		inp_Depto428.click();
		return new SucursalesPage();
	}

}
