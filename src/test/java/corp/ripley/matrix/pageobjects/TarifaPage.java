package corp.ripley.matrix.pageobjects;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

//import cucumber.api.DataTable;


public class TarifaPage extends BasePage{
	
	public @FindBy(xpath = "//*[@id=\'root\']/div/main/div[2]/div[2]/div/div/div[3]/div/div[1]/div/div[2]/div[2]/div/div[1]/div[2]/div/button") WebElement btn_Editar;
	public @FindBy(xpath = "/html/body/div[4]/div[3]/div/div/div[1]/div/div[2]/div[1]/div/div/div/div[1]/div[1]/div/p/b") WebElement btn_Group;
	public @FindBy(xpath = "//*[@id=\'mui-component-select-comunas\']") WebElement sel_Despliegue;
	public @FindBy(xpath = "//*[@id=\'menu-comunas\']/div[3]/ul/li[1]/div/span") WebElement sel_Comunas;
	public @FindBy(xpath = "//*[@id=\'Seleccionar tipo de servicio\']/div") WebElement sel_DespServicio;
	public @FindBy(xpath = "//*[@id=\'menu-\']/div[3]/ul/li[3]") WebElement sel_Servicio;
	public @FindBy(xpath = "//*[@id=\'Seleccionar tamaño\']/div") WebElement sel_DespSize;
	public @FindBy(xpath = "//*[@id=\'menu-\']/div[3]/ul/li[2]") WebElement sel_Size;
	public @FindBy(xpath = "//*[@id=\'Seleccionar criterio de despacho\']/div") WebElement sel_DespCriterio;
	public @FindBy(xpath = "//*[@id=\'menu-\']/div[3]/ul/li[1]") WebElement sel_Criterio;
	public @FindBy(id = "downshift-3-input") WebElement inp_Sku;

	public TarifaPage() throws IOException {
		super();
	}
	
	public void btnEditar() {
		btn_Editar.click();
	}
	
	public void btnGroup() {
		btn_Group.click();
	}
	
	public void selDespliegue() {
		sel_Despliegue.click();
	}
	
	public void selComunas() {
		sel_Comunas.click();
	}
	
	public void selDespServicio() {
		sel_DespServicio.click();
	}
	
	public void selServicio() {
		sel_Servicio.click();
	}
	
	public void selDespSize() {
		sel_DespSize.click();
	}
	
	public void selSize() {
		sel_Size.click();
	}
	
	public void selDespCriterio() {
		sel_DespCriterio.click();
	}
	
	public void selCriterio() {
		sel_Criterio.click();
	}
	
	/*public void inpSku(DataTable dataTable, int row, int column) throws Exception {
		List<List<String>> data = dataTable.raw();
		sendKeysToWebElement(inp_Sku, data.get(row).get(column));
		inp_Sku.click();
	}*/
	

}
