package corp.ripley.matrix.pageobjects;

import java.io.IOException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ServiciosPage extends BasePage{
	
	public @FindBy(xpath = "//*[@id=\'root\']/div/main/div[2]/div[2]/div/div/div[1]/div[2]/div[2]/label/span[1]") WebElement btn_Agregar;
	public @FindBy(xpath = "//*[@id=\'panel1a-content\']/div/div/div[3]/div/div/input") WebElement inp_NomServicio;
	public @FindBy(xpath = "//*[@id=\'panel1a-content\']/div/div/div[4]/div/div/input") WebElement inp_Ocurrencia;
	public @FindBy(xpath = "//*[@id=\'slackDays\']") WebElement inp_Desfase;
	public @FindBy(id = "cutTime_0") WebElement inp_LuHora;
	public @FindBy(id = "cutTime_1") WebElement inp_MaHora;
	public @FindBy(id = "cutTime_2") WebElement inp_MiHora;
	public @FindBy(id = "cutTime_3") WebElement inp_JuHora;
	public @FindBy(id = "cutTime_4") WebElement inp_ViHora;	
	public @FindBy(id = "mui-component-select-Canal de venta") WebElement sel_Canal;
	public @FindBy(xpath = "//*[@id=\'menu-Canal de venta\']/div[3]/ul/li[1]/span[1]/span[1]/input") WebElement sel_CanalTipo;
	public @FindBy(xpath = "(//button[@type='button'])[15]") WebElement btn_Guardar;
	public @FindBy(xpath = "/html/body/div[4]/div[3]/div/div/div[2]/div[1]/button") WebElement btn_Cancelar;
	
	public @FindBy(xpath = "//*[@id=\'root\']/div/main/div[2]/div[2]/div/div/div[1]/div[3]/div/div/div/table/tbody/tr[1]/td[2]/div/span[1]") WebElement btn_Editar;
	public @FindBy(xpath = "//*[@id=\'panel1a-content\']/div/div/div[4]/div/div/div/div/button[1]") WebElement add_Ocurrencia;
	public @FindBy(xpath = "//*[@id=\'panel1a-content\']/div/div/div[4]/div/div/div/div/button[2]") WebElement dism_Ocurrencia;
	public @FindBy(xpath = "//*[@id=\'panel1a-content\']/div/div/div[5]/div/div/div/div/button[1]") WebElement add_Desfase;
	public @FindBy(xpath = "//*[@id=\'panel1a-content\']/div/div/div[5]/div/div/div/div/button[2]") WebElement dism_Desfase;
	
	public @FindBy(xpath = "//*[@id=\'menu-Canal de venta\']/div[3]/ul/li[2]/span[1]/span[1]/input") WebElement unchek_Pos;
	public @FindBy(xpath = "//*[@id=\'menu-Canal de venta\']/div[1]") WebElement get_Focus;
	

	public ServiciosPage() throws IOException {
		super();
	}
	
	public void btnAgregar() {
		btn_Agregar.click();
	}
	
	public void inpNomServicio(String nombreServicio) {
		inp_NomServicio.clear();
		inp_NomServicio.sendKeys(nombreServicio);
	}
	
	public void inpOcurrencia(String Ocurrencia) {
		inp_Ocurrencia.sendKeys(Ocurrencia);
	}
	
	public void inpDesfase(String Desfase) {
		inp_Desfase.sendKeys(Desfase);
	}
	
	public void selCanal() {
		sel_Canal.click();
		sel_CanalTipo.click();
	}
	
	public void inpHorario(String Hora) throws Exception {
		for(int i=0;i<5;i++) {
			inp_LuHora.sendKeys(Keys.BACK_SPACE);
		}
		inp_LuHora.sendKeys(Hora);
		
		for(int i=0;i<5;i++) {
			inp_MaHora.sendKeys(Keys.BACK_SPACE);
		}
		inp_MaHora.sendKeys(Hora);
		
		for(int i=0;i<5;i++) {
			inp_MiHora.sendKeys(Keys.BACK_SPACE);
		}
		inp_MiHora.sendKeys(Hora);
		
		for(int i=0;i<5;i++) {
			inp_JuHora.sendKeys(Keys.BACK_SPACE);
		}
		inp_JuHora.sendKeys(Hora);
		
		for(int i=0;i<5;i++) {
			inp_ViHora.sendKeys(Keys.BACK_SPACE);
		}
		inp_ViHora.sendKeys(Hora);
	}
	
	public void btnGuardar(){
		btn_Guardar.click();
	}
	
	public void btnCancelar() {
		btn_Cancelar.click();
	}
	
	public void btnEditar() {
		btn_Editar.click();
	}
	
	public void editarOcurrencia() {
		inp_Ocurrencia.sendKeys(Keys.BACK_SPACE);
		waitSleep(1);
		add_Ocurrencia.click();
	}
	
	public void editarOcurrenciaMin() {
		dism_Ocurrencia.click();
	}
	
	public void editDesfase() {
		inp_Desfase.sendKeys(Keys.BACK_SPACE);
		waitSleep(1);
		inp_Desfase.sendKeys("1");
	}
	
	public void addDesfase() {
		add_Desfase.click();
	}
	
	public void dismDesfase() {
		dism_Desfase.click();
	}
	
	public void unchekPos() {
		sel_Canal.click();
		waitSleep(1);
		unchek_Pos.click();
		waitSleep(1);
		get_Focus.click();
		waitSleep(1);
	}
	
	public void editHora(String Hora) {
		for(int i=0;i<5;i++) {
			inp_LuHora.sendKeys(Keys.BACK_SPACE);
		}
		inp_LuHora.sendKeys(Hora);
	}



}
