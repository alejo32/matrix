package corp.ripley.matrix.pageobjects;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AgendaPage extends BasePage{
	
	public @FindBy(xpath = "//*[@id=\'root\']/div/main/div[2]/div/div/div/div[3]/div/div[1]/div/div[2]/div[2]/div/div[1]/div[2]/div/button") WebElement btn_Agenda;
	public @FindBy(xpath = "//input[@class=\"jss1219\" and @value=\"M3\"]") WebElement radioButonM3;
	public @FindBy(xpath = "//*[@id=\'panel1a-header\']") WebElement desp_Agenda;

	// Este web elements tiene 3 objetos, por eso se crea una Lista de WebElements
	// Objeto 1, indice 0 --> Nombre de AGenda / Objeto 2, indice 1= fecha incio / Objeto 3, indice 2=fecha fin
	public @FindBy(xpath = "//*[@class=\"MuiInputBase-input MuiOutlinedInput-input MuiInputBase-inputAdornedEnd MuiOutlinedInput-inputAdornedEnd MuiInputBase-inputMarginDense MuiOutlinedInput-inputMarginDense\"]") List<WebElement> inputsCreacionAgenda;

	public @FindBy(xpath = "//*[@id=\'mui-component-select-tipo de servicio\']") WebElement sel_TipoServicio;
	public @FindBy(xpath = "//*[@id=\'menu-tipo de servicio\']/div[3]/ul/li[1]/span[1]/span[1]/input") WebElement inp_Todas;

	// Lista de webelements que mapean el dia de la semana para aumentar capcacidad base, son 7 y van de Lunes a Viernes
	public @FindBy(xpath = "//*[@class=\"MuiButtonBase-root MuiButton-root MuiButton-text jss1335 MuiButton-textSizeSmall MuiButton-sizeSmall\"]") List<WebElement> inputCapacidadBasePorDia;
	public @FindBy(xpath = "//*[@class=\"jss1219\" and @value=\"No\"]") WebElement inputSeleccionNoAutoGenerarAgenda;

	// Lista de webelements que mapea dos botones: 0 --> Cancelar; 1 --> Guardar y Volver
	public @FindBy(xpath = "//*[@class=\"MuiButtonBase-root MuiButton-root MuiButton-outlined jss1163 MuiButton-outlinedPrimary\"]") List<WebElement> btnsCancelarYGuardarYVolver;
	public @FindBy(xpath = "//*[@class=\"MuiButtonBase-root MuiButton-root MuiButton-text jss723 MuiButton-textPrimary\"]") WebElement btnGuardarYContinuar;
	
	public AgendaPage() throws IOException {
		super();
	}
	
	public AgendaPage btnAgenda() throws IOException {
		btn_Agenda.click();	
		return new AgendaPage();
	}
	
	public AgendaPage inpMcub() throws IOException {
		radioButonM3.click();
		return new AgendaPage();
	}
	
	public AgendaPage despAgenda() throws IOException {
		desp_Agenda.click();
	return new AgendaPage();
	}
	
	public AgendaPage pongoNombreDeAgenda() throws IOException {
		for(int i=0; i<=7; i++) {
			inputsCreacionAgenda.get(0).sendKeys(Keys.BACK_SPACE);
	    }
		inputsCreacionAgenda.get(0).sendKeys("Agenda de Prueba");
		return new AgendaPage();
	}
	
	public AgendaPage selTipoServicio() throws IOException {
		sel_TipoServicio.click();
		return new AgendaPage();
	}
	
	public AgendaPage inpTodas() throws IOException {
		inp_Todas.click();
		return new AgendaPage();
	}
	
	public AgendaPage seleccionoFechaDeinicio() throws IOException {
		Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = dateFormat.format(date);
		System.out.println(strDate);
		inputsCreacionAgenda.get(1).sendKeys(strDate);

		return new AgendaPage();
	}
	
	public AgendaPage seleccionoFechaDeFin() throws IOException {
		Calendar c = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		c.add(Calendar.DAY_OF_MONTH, 5);
		Date date = c.getTime();
		String strDate = dateFormat.format(date);
		System.out.println(strDate);

		inputsCreacionAgenda.get(2).sendKeys(strDate);

		return new AgendaPage();
	}

	/**
	 * Metodo que permite incrementar en 1 dia la capacidad base
	 * @return el metodo
	 * @throws IOException asd
	 */
	public AgendaPage incrementoLaCapacidadBasePorDia() throws IOException {
		inputCapacidadBasePorDia.get(0).click(); //--> Lunes
		inputCapacidadBasePorDia.get(1).click(); //--> etc...
		inputCapacidadBasePorDia.get(2).click();
		inputCapacidadBasePorDia.get(3).click();
		inputCapacidadBasePorDia.get(4).click();

		return new AgendaPage();
	}
	
	public AgendaPage rdbNo() throws IOException {
		inputSeleccionNoAutoGenerarAgenda.click();
		return new AgendaPage();
	}
	
	public AgendaPage btnSaveBack() throws IOException {
		btnsCancelarYGuardarYVolver.get(1).click();
		return new AgendaPage();
	}

	public AgendaPage presionoBotonGuardarYContinuarPostCreacionDeAgenda() throws IOException {
		btnGuardarYContinuar.click();
		return new AgendaPage();
	}
}
