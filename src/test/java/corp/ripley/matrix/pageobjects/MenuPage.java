package corp.ripley.matrix.pageobjects;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class MenuPage extends BasePage{
	
	public @FindBy(xpath = "//*[@id=\'root\']/div/div/div/ul/div[1]/a/div[2]") 			WebElement menu_Panel;
	public @FindBy(xpath = "//*[@id=\"root\"]/div/div/div/ul/div[1]/div[1]/div[2]/span") WebElement menu_Flujos;
	public @FindBy(xpath = "//*[@id=\'root\']/div/div/div/ul/div[1]/div[2]/div/div/a/div/span")WebElement sub_DDC;
	public @FindBy(xpath = "//*[@id=\'root\']/div/div/div/ul/div[3]/div[1]/div[2]/span") WebElement sub_Agenda;
	public @FindBy(xpath = "//*[@id=\'root\']/div/div/div/ul/div[4]/a/div[2]/span") WebElement simu_Agenda;
	public @FindBy(xpath = "//*[@class=\"MuiTypography-root MuiLink-root MuiLink-underlineHover MuiTypography-colorPrimary\" and @href=\"/product-schedule/1\"]") WebElement lnk_ProdRipley;
	
	public @FindBy(xpath = "//*[@id=\'Despacho_a_domicilio\']") WebElement desp_Menu;
	public @FindBy(xpath = "//*[@id=\'root\']/div/div/div/ul/div[2]/div/div[2]/span") WebElement desp_VarDesp;
	public @FindBy(xpath = "//*[@id=\'root\']/div/div/div/ul/div[1]/div[1]/div[2]/span") WebElement desp_Flujo;
	public @FindBy(xpath = "//*[@id=\'root\']/div/div/div/ul/div[1]/div[2]/div/div/a/div/span") WebElement desp_DDR;
	public @FindBy(xpath = "//*[@id=\'root\']/div/div/div/ul/div[2]/div[2]/div/div/a/div/span") WebElement tipo_Servicio;
	public @FindBy(xpath = "//*[@id=\'root\']/div/div/div/ul/div[2]/div[3]/div/div/a/div/span") WebElement tipo_OPL;
	public @FindBy(xpath = "//*[@id=\'root\']/div/div/div/ul/div[2]/div[4]/div/div/a/div/span") WebElement tipo_Tarifa;
	public @FindBy(xpath = "//*[@id=\'root\']/div/div/div/ul/div[3]/div[2]/div/div/a/div/span") WebElement agen_Desp;
	
	public @FindBy(xpath = "//*[@id=\'root\']/div/main/div[2]/div[2]/div/div/div[3]/div/div[1]/div/div[2]/div[2]") WebElement menu_Close;
	public @FindBy(xpath = "//*[@id=\'root\']/div/main/div[2]") WebElement esc_Servicio;

	public MenuPage() throws IOException {
		super();
	}
	
	public MenuPage menuPanel() throws IOException {
		menu_Panel.click();
		return new MenuPage();
	}
	
	public MenuPage menuFlujos() throws IOException {
		menu_Flujos.click();
	return new MenuPage();
	}
	
	public MenuPage subDespacho() throws IOException {
		sub_DDC.click();
	return new MenuPage();
	}
	
	public void despFlujoDespacho() {
		Actions action = new Actions(driver);
		action.moveToElement(desp_Menu).moveToElement(desp_Flujo).click().build().perform();
		BasePage.waitSleep(1);		
	}
	
	public void despDDR() {
		Actions action = new Actions(driver);
		action.moveToElement(desp_Menu).moveToElement(desp_DDR).click().build().perform();
		BasePage.waitSleep(1);
	}
	
	public void despVarDespacho() {
		Actions action = new Actions(driver);
		action.moveToElement(desp_Menu).moveToElement(desp_VarDesp).click().build().perform();
		BasePage.waitSleep(1);		
	}
	
	public void subAgenda() {
		Actions action = new Actions(driver);
		action.moveToElement(desp_Menu).moveToElement(sub_Agenda).click().build().perform();
		BasePage.waitSleep(1);
	}
	
	public void simuAgenda() {
		Actions action = new Actions(driver);
		action.moveToElement(desp_Menu).moveToElement(simu_Agenda).click().build().perform();
		BasePage.waitSleep(1);
	}
	
	public void tipoServicio() {
		Actions action = new Actions(driver);
		action.moveToElement(desp_Menu).moveToElement(tipo_Servicio).click().build().perform();
		BasePage.waitSleep(1);
	}
	
	public void tipoOPL() {
		Actions action = new Actions(driver);
		action.moveToElement(desp_Menu).moveToElement(tipo_OPL).click().build().perform();
		BasePage.waitSleep(1);
	}
	
	public void tipoTarifa() {
		Actions action = new Actions(driver);
		action.moveToElement(desp_Menu).moveToElement(tipo_Tarifa).click().build().perform();
		BasePage.waitSleep(1);
	}
	
	public void agendaDesp() {
		Actions action = new Actions(driver);
		action.moveToElement(desp_Menu).moveToElement(agen_Desp).click().build().perform();
		BasePage.waitSleep(1);
	}

	public void ClickProductosRipley() {
		BasePage.waitSleep(3);
		lnk_ProdRipley.click();
	}
	
	public void menuClose(){
		menu_Close.click();
	}
	
	public void escServicio() {
		esc_Servicio.click();
	}

	
	
	
	
	
	

}
