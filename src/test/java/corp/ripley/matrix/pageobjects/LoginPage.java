package corp.ripley.matrix.pageobjects;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class LoginPage extends BasePage {
	
	public @FindBy(name = "email") 																WebElement user_Name;
	public @FindBy(name = "password")															WebElement user_Pass;
	public @FindBy(xpath = "//*[@id=\'login-button\']")											WebElement btn_Inicio;
	public @FindBy(xpath = "//*[@id=\'email-helper-text\']") 									WebElement txt_MailFail;
	public @FindBy(xpath = "/html/body/div/div/main/div/main/div[2]/div/div/div/div[1]/span")	WebElement txt_PassFail;

	public LoginPage() throws IOException {
		super();
	}
	
	public LoginPage getMTOAlphaWeb() throws IOException {
		getDriver().get("https://mtx.ripleyqa.com/");
		return new LoginPage();
	}
	
	public LoginPage userName(String userName) throws Exception {
		sendKeysToWebElement(user_Name, userName);
		return new LoginPage();
	}
	
	public LoginPage userPass(String password) throws Exception {
		sendKeysToWebElement(user_Pass, password);
		return new LoginPage();
	}
	
	public LoginPage btnInicio() throws Exception {
		btn_Inicio.click();
		return new LoginPage();
	}
	
	public LoginPage msjMailFail() throws Exception {
		Assert.assertEquals("Correo ripley no válido", txt_MailFail.getText());	
		return new LoginPage();
	}
	
	public LoginPage msjPassFail() throws Exception {
		Assert.assertEquals("La contraseña y/o usuario es invalida.", txt_PassFail.getText());	
		return new LoginPage();
	}
}
