package corp.ripley.matrix.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import corp.ripley.matrix.pageobjects.BasePage;
import corp.ripley.matrix.utils.DriverFactory;

public class ZonaSteps extends DriverFactory{
	
	@Given("^Despliego opción de Nueva Zona$")
	public void despliego_opción_de_Nueva_Zona() throws Throwable {
	    zonaPage.nuevaZona();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Ingreso el Nombre de la Nueva Zona$")
	public void ingreso_el_Nombre_de_la_Nueva_Zona() throws Throwable {
	    zonaPage.txtZona();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Selecciono la Región Metropolitana$")
	public void selecciono_la_Región_Metropolitana() throws Throwable {
	    zonaPage.selRegion();
	    BasePage.waitSleep(1);
	    zonaPage.selMetrop();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Selecciono todas las comunas$")
	public void selecciono_todas_las_comunas() throws Throwable {
	    zonaPage.selComunas();
	    BasePage.waitSleep(1);
	    zonaPage.selTodas();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}
	
	@When("^Selecciono Departamento Lima$")
	public void selecciono_Departamento_Lima() throws Throwable {
	    zonaPage.selDepto();
	    BasePage.waitSleep(1);
	    zonaPage.selDeptoLima();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Selecciono todas las Provincias$")
	public void selecciono_todas_las_Provincias() throws Throwable {
	    zonaPage.selProvi();
	    BasePage.waitSleep(1);
	    zonaPage.selProviTodas();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Selecciono todos los Distritos$")
	public void selecciono_todos_los_Distritos() throws Throwable {
		zonaPage.selDist();
		BasePage.waitSleep(1);
		zonaPage.selDistTodas();
		BasePage.waitSleep(2);
		BasePage.takeScreenShot();
	}

    @Then("^Presiono el Botón Guardar y Continuar de Zona$")
    public void presionoElBotónGuardarYContinuarDeZona() throws Throwable{
		zonaPage.presionoBotonGuardarYContinuarEnZona();
    }
}
