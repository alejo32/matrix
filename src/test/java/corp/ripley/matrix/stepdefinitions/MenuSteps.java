package corp.ripley.matrix.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import corp.ripley.matrix.pageobjects.BasePage;
import corp.ripley.matrix.utils.DriverFactory;

public class MenuSteps extends DriverFactory {
	
	@Given("^Hago click en opción Paneles de Control$")
	public void hago_click_en_opción_Paneles_de_Control() throws Throwable {
	    menuPage.menuPanel();
	    BasePage.waitSleep(2);
	}

	@Then("^Se despliega Pantalla Dashboard$")
	public void se_despliega_Pantalla_Dashboard() throws Throwable {
	    BasePage.takeScreenShot();
	}

	@Given("^Hago click en opción Flujos de Despacho$")
	public void hago_click_en_opción_Flujos_de_Despacho() throws Throwable {
		menuPage.despFlujoDespacho();
	}

	@When("^Hago click en opción Despacho Directo a Cliente$")
	public void hago_click_en_opción_Despacho_Directo_a_Cliente() throws Throwable {
		menuPage.despDDR();
	    BasePage.takeScreenShot();
	}
	
	/*@Then("^cierro menú lateral$")
	public void cierro_menú_lateral() throws Throwable {
	    menuPage.menuClose();
	    BasePage.takeScreenShot();
	}*/
	
	@Given("^Hago click en opción Variables Despacho$")
	public void hago_click_en_opción_Variables_Despacho() throws Throwable {
		menuPage.despVarDespacho();
	}

	@When("^Hago click en opción Tipos de Servicio$")
	public void hago_click_en_opción_Tipos_de_Servicio() throws Throwable {
	    menuPage.tipoServicio();
	}
	
	@Then("^cierro menú lateral de Variables de Despacho$")
	public void cierro_menú_lateral_de_Variables_de_Despacho() throws Throwable {
	    menuPage.escServicio();
	    BasePage.waitSleep(2);
	}
	
	@When("^Hago click en opción Tipos de Servicio por OPL$")
	public void hago_click_en_opción_Tipos_de_Servicio_por_OPL() throws Throwable {
	    menuPage.tipoOPL();	    
	}

	@Then("^cierro menú lateral de Servicios por Operador Logistico$")
	public void cierro_menú_lateral_de_Servicios_por_Operador_Logistico() throws Throwable {
	    menuPage.escServicio();
	}
	
	@When("^Hago click en opción Tarifas de Despacho$")
	public void hago_click_en_opción_Tarifas_de_Despacho() throws Throwable {
	    menuPage.tipoTarifa();	    
	}

	@Then("^cierro menú lateral de Tarifas de Despacho$")
	public void cierro_menú_lateral_de_Tarifas_de_Despacho() throws Throwable {
	    menuPage.escServicio();
	}
	
	@Given("^Hago click en opción Agendas$")
	public void hago_click_en_opción_Agendas() throws Throwable {
	    menuPage.subAgenda();
	}

	@When("^Hago click en opción Agendas de Despacho$")
	public void hago_click_en_opción_Agendas_de_Despacho() throws Throwable {
	    menuPage.agendaDesp();
	}

	@Then("^cierro menú lateral de Agendas$")
	public void cierro_menú_lateral_de_Agendas() throws Throwable {
	    menuPage.escServicio();
	}
	
	@Given("^Hago click en opción Simulador Agenda$")
	public void hago_click_en_opción_Simulador_Agenda() throws Throwable {
	    menuPage.simuAgenda();
	}

	@Then("^cierro menú lateral$")
	public void cierro_menú_lateral() throws Throwable {
		menuPage.escServicio();
		BasePage.takeScreenShot();
	}

	@Given("^Ingreso a crear agenda Productos Ripley$")
	public void ingreso_a_crear_agenda_Productos_Ripley() throws Throwable {
		menuPage.ClickProductosRipley();
	}


}
