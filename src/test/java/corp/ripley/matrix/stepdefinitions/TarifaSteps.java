package corp.ripley.matrix.stepdefinitions;

//import cucumber.api.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import corp.ripley.matrix.pageobjects.BasePage;
import corp.ripley.matrix.utils.DriverFactory;

public class TarifaSteps extends DriverFactory{
	
	@Given("^Presiono Boton para editar nueva zona$")
	public void presiono_Boton_para_editar_nueva_zona() throws Throwable {
	    tarifaPage.btnEditar();
	    BasePage.waitSleep(2);
	}

	@Given("^En pantalla Asigno tarifa a Grupo Uno$")
	public void en_pantalla_Asigno_tarifa_a_Grupo_Uno() throws Throwable {
	    tarifaPage.btnGroup();
	    BasePage.waitSleep(2);
	    tarifaPage.selDespliegue();
	    BasePage.waitSleep(1);
	    tarifaPage.selComunas();
	    BasePage.waitSleep(1);
	}

	@When("^Selecciono Tipo de Servicio$")
	public void selecciono_Tipo_de_Servicio() throws Throwable {
		tarifaPage.selDespServicio();
		BasePage.waitSleep(1);
	    tarifaPage.selServicio();
	    BasePage.waitSleep(1);
	}

	@When("^Selecciono Tamaño$")
	public void selecciono_Tamaño() throws Throwable {
		tarifaPage.selDespSize();
		BasePage.waitSleep(1);
	    tarifaPage.selSize();
	    BasePage.waitSleep(2);
	}

	@When("^Selecciono el Criterio de despacho$")
	public void selecciono_el_Criterio_de_despacho() throws Throwable {
		tarifaPage.selDespCriterio();
		BasePage.waitSleep(2);
		tarifaPage.selCriterio();
		BasePage.waitSleep(1);
	}
	
	/*@When("^Ingreso <SKU> valido para el Grupo$")
	public void ingreso_SKU_valido_para_el_Grupo(DataTable sku) throws Throwable {
		tarifaPage.inpSku(sku,0,0);
		BasePage.waitSleep(2);
	}*/

//	@When("^Ingreso <SKU> valido para el Grupo$")
//	public void ingreso_SKU_valido_para_el_Grupo() throws Throwable {
//	    
//	}

	@Then("^Presiono el boton Guardar y Volver$")
	public void presiono_el_boton_Guardar_y_Volver() throws Throwable {
	    
	}

	@Then("^Presiono el boton Guardar y Continuar$")
	public void presiono_el_boton_Guardar_y_Continuar() throws Throwable {
	   
	}

}
