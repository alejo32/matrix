package corp.ripley.matrix.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import corp.ripley.matrix.pageobjects.BasePage;
import corp.ripley.matrix.utils.DriverFactory;

import java.io.IOException;

public class OperadorSteps extends DriverFactory {
	
	@Given("^Ingreso nuevo OPL Redex$")
	public void ingreso_nuevo_OPL_Redex() throws Throwable {
	    operadorPage.txtOPL();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@Given("^Selecciono opción recomendada$")
	public void selecciono_opción_recomendada() throws Throwable {
	    operadorPage.txtOPLSel();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Se despliega la información correspondiente al OPL seleccionado$")
	public void se_despliega_la_información_correspondiente_al_OPL_seleccionado() throws Throwable {
	    BasePage.takeScreenShot();
	    operadorPage.scrollOPL();
	    BasePage.waitSleep(2);
	}

	@Then("^Presiono el Botón Guardar y Continuar de OPL$")
	public void presionoElBotónGuardarYContinuarDeOPL() throws IOException {
		BasePage.takeScreenShot();
		operadorPage.hagoClickEnBotonGuardarDeOPL();
		BasePage.waitSleep(2);
		BasePage.takeScreenShot();
	}
	
	@Given("^Ingreso nuevo OPL Trujillo$")
	public void ingreso_nuevo_OPL_Trujillo() throws Throwable {
		operadorPage.txtOPLPE();
	    BasePage.waitSleep(2);
	    operadorPage.txtOPLSelPE();	    
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	    
	}
}
