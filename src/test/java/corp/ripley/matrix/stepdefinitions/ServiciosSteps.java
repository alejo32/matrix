package corp.ripley.matrix.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import corp.ripley.matrix.pageobjects.BasePage;
import corp.ripley.matrix.utils.DriverFactory;

public class ServiciosSteps extends DriverFactory{
	
	@Given("^Presiono icono Agregar Nuevo para ver Pantalla Agregar Tipo de Servicio$")
	public void presiono_icono_Agregar_Nuevo_para_ver_Pantalla_Agregar_Tipo_de_Servicio() throws Throwable {
	    serviciosPage.btnAgregar();
	}

	@When("^Ingreso Nombre de Servicio \"([^\"]*)\"$")
	public void ingreso_Nombre_de_Servicio(String nombreServicio) throws Throwable {
		serviciosPage.inpNomServicio(nombreServicio);
	}

	@When("^Ingreso ocurrencia Maxima \"([^\"]*)\"$")
	public void ingreso_ocurrencia_Maxima(String Ocurrencia) throws Throwable {
	    serviciosPage.inpOcurrencia(Ocurrencia);
	}

	@When("^Ingreso dias de desface \"([^\"]*)\"$")
	public void ingreso_dias_de_desface(String Desfase) throws Throwable {
		serviciosPage.inpDesfase(Desfase);	    
	}

	@When("^Selecciono Canal de Ventas Todos$")
	public void selecciono_Canal_de_Ventas_Todos() throws Throwable {
		serviciosPage.selCanal();
	}

	@Then("^Seleccionamos horario de corte \"([^\"]*)\" Lu-Vi$")
	public void seleccionamos_horario_de_corte_Lu_Vi(String Hora) throws Throwable {
		serviciosPage.inpHorario(Hora);
		BasePage.waitSleep(2);
	}

	@Then("^Presiono botón Guardar y Volver$")
	public void Presiono_botón_Guardar_y_Volver() throws Throwable {
	    serviciosPage.btnGuardar();
	    BasePage.waitSleep(2);
	}
	
	@Given("^Presiono icono Editar para ver Pantalla Agregar Tipo de Servicio$")
	public void presiono_icono_Editar_para_ver_Pantalla_Agregar_Tipo_de_Servicio() throws Throwable {
	    serviciosPage.btnEditar();
	    BasePage.waitSleep(1);
	}

	@When("^Modifico la ocurrencia Maxima a \"([^\"]*)\"$")
	public void modifico_la_ocurrencia_Maxima_a(String arg1) throws Throwable {
	    serviciosPage.editarOcurrencia();
	    BasePage.waitSleep(2);
	}
	
	@When("^Modifico la ocurrencia quedando en \"([^\"]*)\"$")
	public void modifico_la_ocurrencia_quedando_en(String arg1) throws Throwable {
		serviciosPage.editarOcurrenciaMin();
	}
	
	@When("^Editar días de desfase$")
	public void editar_días_de_desfase() throws Throwable {
	    serviciosPage.editDesfase();
	}
	
	@When("^Editar días de desfase aumentandolo$")
	public void editar_días_de_desfase_aumentandolo() throws Throwable {
	    serviciosPage.addDesfase();
	}
	
	@When("^Editar días de desfase disminuyendo$")
	public void editar_días_de_desfase_disminuyendo() throws Throwable {
	    serviciosPage.dismDesfase();
	}
	
	@When("^Saco ckeck de Canal POS$")
	public void saco_ckeck_de_Canal_POS() throws Throwable {
	    serviciosPage.unchekPos();
	}
	
	@When("^Modificamos horario de corte \"([^\"]*)\" Lunes$")
	public void modificamos_horario_de_corte_Lunes(String Hora) throws Throwable {
	    serviciosPage.editHora(Hora);
	}



}
