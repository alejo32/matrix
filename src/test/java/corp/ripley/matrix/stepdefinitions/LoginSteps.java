package corp.ripley.matrix.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import corp.ripley.matrix.utils.DriverFactory;

public class LoginSteps extends DriverFactory {
	
	@Given("^Accedo a Web Matrix$")
	public void accedo_a_Web_Matrix() throws Throwable {
		loginPage.getMTOAlphaWeb();
	}

	@Given("^Ingresar Usuario Matrix CL$")
	public void ingresar_Usuario_Matrix_CL() throws Throwable {
		loginPage.userName("chile@ripley.com");
	}

	@Given("^Ingreso password Matrix CL$")
	public void ingreso_password_Matrix_CL() throws Throwable {
	    loginPage.userPass("123456");
	}

	@When("^Hago click en Botón Iniciar Sesión$")
	public void hago_click_en_Botón_Iniciar_Sesión() throws Throwable {
	    loginPage.btnInicio();
	}

	@Then("^Se despliega Dashboard de web Matrix$")
	public void se_despliega_Dashboard_de_web_Matrix() throws Throwable {
		System.out.println("Mostrando dashboard web de Matrix");
	}
	
	@Given("^Ingresar Usuario Matrix PE$")
	public void ingresar_Usuario_Matrix_PE() throws Throwable {
		loginPage.userName(getUserPE());
	}

	@Given("^Ingreso password Matrix PE$")
	public void ingreso_password_Matrix_PE() throws Throwable {
		loginPage.userPass(getPassCL());
	}
	
	@Given("^Ingresar Usuario inválido$")
	public void ingresar_Usuario_inválido() throws Throwable {
		loginPage.userName(getUserFail());
	}

	@Then("^Se despliega mensaje de alerta Correo Ripley no válido$")
	public void se_despliega_mensaje_de_alerta_Correo_Ripley_no_válido() throws Throwable {
		loginPage.msjMailFail();
	}

	@Given("^Ingreso password inválida$")
	public void ingreso_password_inválida() throws Throwable {
		loginPage.userPass(getPassFail());
	}

	@Then("^Se despliega mensaje de alerta Contraseña no válida$")
	public void se_despliega_mensaje_de_alerta_Contraseña_no_válida() throws Throwable {
	    loginPage.msjPassFail();
	}

}
