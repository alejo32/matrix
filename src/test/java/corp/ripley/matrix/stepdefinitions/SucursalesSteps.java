package corp.ripley.matrix.stepdefinitions;

//import cucumber.api.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import corp.ripley.matrix.pageobjects.BasePage;
import corp.ripley.matrix.utils.DriverFactory;

public class SucursalesSteps extends DriverFactory {
	
	@Given("^Selecciono una o más sucursales$")
	public void selecciono_una_o_más_sucursales() throws Throwable {
	    sucursalesPage.txtSucursal();
	    BasePage.waitSleep(2);
	    sucursalesPage.txtSucursalSel();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Editamos la Configuración$")
	public void editamos_la_Configuración() throws Throwable {
	    sucursalesPage.btnEditar();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Se despliega pantalla para Asociar Sucursal de Stock$")
	public void se_despliega_pantalla_para_Asociar_Sucursal_de_Stock() throws Throwable {
	    BasePage.takeScreenShot();
	}

	@When("^Despliego opción de edición de Relación$")
	public void despliego_opción_de_edición_de_Relación() throws Throwable {
	    sucursalesPage.expRelacion();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^En criterio de Despacho selecciono Canal de Ventas$")
	public void en_criterio_de_Despacho_selecciono_Canal_de_Ventas() throws Throwable {
	    sucursalesPage.selCanal();
	    BasePage.waitSleep(1);
	    sucursalesPage.selCanalTodo();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Selecciono Tipos de Servicio$")
	public void selecciono_Tipos_de_Servicio() throws Throwable {
	    sucursalesPage.selTipoSRC();
	    BasePage.waitSleep(1);
	    sucursalesPage.selServTodo();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^En Criterios de Producto selecciono Departamento (\\d+)$")
	public void en_Criterios_de_Producto_selecciono_Departamento(int arg1) throws Throwable {
	    sucursalesPage.menuProd();
	    BasePage.waitSleep(1);
	    sucursalesPage.selDepto();
	    BasePage.waitSleep(1);
	    sucursalesPage.selDepto179();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	    sucursalesPage.divFoco();
	    BasePage.waitSleep(1);
	}

	@When("^En seleccionar Línea selecciono Todas$")
	public void en_seleccionar_Línea_selecciono_Todas() throws Throwable {
	    sucursalesPage.selLinea();
	    BasePage.waitSleep(1);
	    sucursalesPage.selLineaTodos();
	    BasePage.waitSleep(2);
	    sucursalesPage.divFocoLinea();
	    BasePage.takeScreenShot();
	}
	
	@When("^selecciono sublinea de acuerdo a seleccion anterior$")
	public void selecciono_sublinea_de_acuerdo_a_seleccion_anterior() throws Throwable {
		BasePage.waitSleep(1);
		sucursalesPage.selSubLinea();
		BasePage.waitSleep(1);
		sucursalesPage.selSublinea523115();
		BasePage.waitSleep(2);
		sucursalesPage.divFocoSub();
		BasePage.waitSleep(2);
	}
	
	/*@When("^Ingreso <SKU> a consultar$")
	public void ingreso_SKU_a_consultar(DataTable sku) throws Throwable {
		sucursalesPage.inpSKU(sku,0,0);
		BasePage.waitSleep(2);
	}*/

	@When("^Presiono el botón Insertar$")
	public void presiono_el_botón_Insertar() throws Throwable {
	    sucursalesPage.btnInsertar();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@Then("^Se carga la información correspondiente en la Tabla de registro$")
	public void se_carga_la_información_correspondiente_en_la_Tabla_de_registro() throws Throwable {
	   sucursalesPage.scrollTabla();
	   BasePage.waitSleep(2);
	   BasePage.takeScreenShot();
	}
	
	@Given("^Selecciono Sucursal Lima$")
	public void selecciono_Sucursal_Lima() throws Throwable {
	    sucursalesPage.txtSucursalPE();
	    BasePage.waitSleep(1);
	    sucursalesPage.txtSucursalSelPE();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}
	
	@When("^En Criterios de Producto selecciono Departamento Marcas Sport (\\d+)$")
	public void en_Criterios_de_Producto_selecciono_Departamento_Marcas_Sport(int arg1) throws Throwable {
		sucursalesPage.menuProd();
		BasePage.waitSleep(2);
		sucursalesPage.selDepto();
		BasePage.waitSleep(2);
	    sucursalesPage.selDepto428();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

}
