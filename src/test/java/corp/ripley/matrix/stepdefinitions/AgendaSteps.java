package corp.ripley.matrix.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;
import corp.ripley.matrix.pageobjects.BasePage;
import corp.ripley.matrix.utils.DriverFactory;

import java.io.IOException;

	public class AgendaSteps extends DriverFactory {
	
	@Given("^Despliego Pantalla (\\d+) Creación de Agenda$")
	public void despliego_Pantalla_Creación_de_Agenda(int arg1) throws Throwable {
		agendaPage.btnAgenda();
		BasePage.waitSleep(2);
		BasePage.takeScreenShot();
		
	}

	@When("^Selecciono Unidad de Medida en M(\\d+)$")
	public void selecciono_Unidad_de_Medida_en_M(int arg1) throws Throwable {
	    agendaPage.inpMcub();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Despliego información de Agenda$")
	public void despliego_información_de_Agenda() throws Throwable {
	    agendaPage.despAgenda();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@Then("^Genero un nombre a la agenda$")
	public void genero_un_nombre_a_la_agenda() throws Throwable {
	    agendaPage.pongoNombreDeAgenda();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@Then("^Selecciono el tipo de servicio, como TODAS$")
	public void selecciono_el_tipo_de_servicio_como_TODAS() throws Throwable {
	    agendaPage.selTipoServicio();
	    BasePage.waitSleep(1);
	    agendaPage.inpTodas();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@Then("^Ingreso una fecha de inicio$")
	public void ingreso_una_fecha_de_inicio() throws Throwable {
		agendaPage.seleccionoFechaDeinicio();
	    BasePage.waitSleep(2);	    
	    BasePage.takeScreenShot();
	}

	@Then("^Ingreso una fecha de termino$")
	public void ingreso_una_fecha_de_termino() throws Throwable {
		agendaPage.seleccionoFechaDeFin();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@Then("^Agrego capacidad base por día$")
	public void agrego_capacidad_base_por_día() throws Throwable {
	    agendaPage.incrementoLaCapacidadBasePorDia();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@Then("^Seleccionamos no auto generar Agenda$")
	public void seleccionamos_no_auto_generar_Agenda() throws Throwable {
	    agendaPage.rdbNo();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@Then("^Presiono el Botón Guardar y Volver$")
	public void presiono_el_Botón_Guardar_y_Volver() throws Throwable {
	    agendaPage.btnSaveBack();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}


	@And("^Presiono el Botón Guardar y Continuar de Agenda$")
	public void presionoElBotónGuardarYContinuarDeAgenda() throws IOException {
		agendaPage.presionoBotonGuardarYContinuarPostCreacionDeAgenda();
		BasePage.waitSleep(2);
		BasePage.takeScreenShot();
	}
}
