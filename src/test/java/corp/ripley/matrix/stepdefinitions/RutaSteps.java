package corp.ripley.matrix.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;
import corp.ripley.matrix.pageobjects.BasePage;
import corp.ripley.matrix.utils.DriverFactory;

import java.io.IOException;

public class RutaSteps extends DriverFactory {
	
	@Given("^Despliego opción para agregar Grupo$")
	public void despliego_opción_para_agregar_Grupo() throws Throwable {
	    BasePage.waitSleep(2);
		rutaPage.nuevoGrupo();
		BasePage.waitSleep(1);
		BasePage.takeScreenShot();
	}

	@When("^Realizo click en botón Añadir Grupo$")
	public void realizo_click_en_botón_Añadir_Grupo() throws Throwable {
	    rutaPage.addGrupo();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Selecciono todas las comunas del Grupo (\\d+)$")
	public void selecciono_todas_las_comunas_del_Grupo(int arg1) throws Throwable {
	    rutaPage.selComunas();
	    BasePage.waitSleep(2);
	    rutaPage.selTodas();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Selecciono el formato de plazo como Día$")
	public void selecciono_el_formato_de_plazo_como_Día() throws Throwable {
	    rutaPage.selFormato();
	    BasePage.waitSleep(2);
	    rutaPage.selDia();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Selecciono el Plazo dejando configurado (\\d+) días$")
	public void selecciono_el_Plazo_dejando_configurado_días(int arg1) throws Throwable {
	    rutaPage.addDia();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Selecciono Frecuencia Lu, Ma y Mi$")
	public void selecciono_Frecuencia_Lu_Ma_y_Mi() throws Throwable {
	    rutaPage.seleccionoLunesMartesYMiercoles();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Selecciono Volumen Mínimo Chico$")
	public void selecciono_Volumen_Mínimo_Chico() throws Throwable {
	    rutaPage.selVolMin();
	    BasePage.waitSleep(2);
	}

	@When("^Selecciono Volumen Máximo Mediano$")
	public void selecciono_Volumen_Máximo_Mediano() throws Throwable {
	    rutaPage.selVolMax();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@Then("^Presiono el Aceptar$")
	public void presiono_el_Aceptar() throws Throwable {
	    rutaPage.btnAceptar();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}
	
	@When("^Selecciono todas las Provincias del Grupo (\\d+)$")
	public void selecciono_todas_las_Provincias_del_Grupo(int arg1) throws Throwable {
	    rutaPage.selProv();
	    BasePage.waitSleep(1);
	    rutaPage.selProvTodas();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Selecciono todos los distritos$")
	public void selecciono_todos_los_distritos() throws Throwable {
	    rutaPage.selDist();
	    BasePage.waitSleep(1);
	    rutaPage.selDistTodas();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@And("^Presiono el Boton Guardar y Continuar de Ruta$")
	public void presionoElBotonGuardarYContinuarDeRuta() throws IOException, InterruptedException {
		rutaPage.presionoBotonGuardarYContinuarDeRuta();
		Thread.sleep(2000);
	}
}
