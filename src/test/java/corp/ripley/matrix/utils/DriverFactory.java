package corp.ripley.matrix.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

import corp.ripley.matrix.pageobjects.AgendaPage;
import corp.ripley.matrix.pageobjects.LoginPage;
import corp.ripley.matrix.pageobjects.MenuPage;
import corp.ripley.matrix.pageobjects.OperadorPage;
import corp.ripley.matrix.pageobjects.RutaPage;
import corp.ripley.matrix.pageobjects.ServiciosPage;
import corp.ripley.matrix.pageobjects.SucursalesPage;
import corp.ripley.matrix.pageobjects.TarifaPage;
import corp.ripley.matrix.pageobjects.ZonaPage;

public class DriverFactory {
	public static WebDriver driver;
	public static LoginPage loginPage;
	public static MenuPage menuPage;
	public static OperadorPage operadorPage;
	public static ZonaPage zonaPage;
	public static RutaPage rutaPage;
	public static AgendaPage agendaPage;
	public static SucursalesPage sucursalesPage;
	public static TarifaPage tarifaPage;
	public static ServiciosPage serviciosPage;

	public WebDriver getDriver() {

		String webdriver = System.getProperty("browser", "chrome");

		try {
			/*ReadConfigFile file = new ReadConfigFile();
			String browserName = file.getBrowser();
			System.out.println("Obteniendo valor del driver: " + browserName);*/

			switch (webdriver) {
				case "firefox":
					if (null == driver) {
						WebDriverManager.firefoxdriver().setup();
						//FirefoxOptions fo = new FirefoxOptions();
						//fo.addArguments("--headless");
						driver = new FirefoxDriver();
					}
					break;
				case "chrome":
					if (null == driver) {
						WebDriverManager.chromedriver().setup();
						ChromeOptions co = new ChromeOptions();
						co.addArguments("--no-sandbox");
						co.addArguments("--disable-dev-shm-usage");
						//co.addArguments("--headless");
						/*co.addArguments("--disable-gpu");
						co.addArguments("--remote-debugging-port=9222");
						co.addArguments("--start-maximized");
						co.addArguments("--ignore-certificate-errors");
						co.addArguments("--disable-popup-blocking");
						co.addArguments("--window-size=1920,1080");
						co.addArguments("--incognito");*/
						driver = new ChromeDriver(co);
						driver.manage().window().maximize();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					}
					break;
				case "bitbucket":
					if (null == driver) {
						WebDriverManager.chromedriver().setup();
						ChromeOptions co = new ChromeOptions().setHeadless(true);
						co.addArguments("--no-sandbox");
						co.addArguments("--disable-dev-shm-usage");
						driver = new ChromeDriver(co);
						driver.manage().window().maximize();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					}
					break;
				case "ie":
					if (null == driver) {
						// Todo
					}
					break;
				case "remote":
					if (null == driver) {
						String host = "10.0.149.90";
						String completeUrl = "http://" + host + ":4444";

						WebDriverManager.chromedriver().setup();
						ChromeOptions cop = new ChromeOptions();

						cop.addArguments("--no-sandbox");
						cop.addArguments("--disable-dev-shm-usage");
						cop.addArguments("--incognito");

						try {
							driver = new RemoteWebDriver(new URL(completeUrl), cop);
							driver.manage().window().maximize();
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						} catch (MalformedURLException e) {
							e.printStackTrace();
						}
					}
					break;
			}
		} catch (Exception e) {
			System.out.println("No se cargo browser: " + e.getMessage());
		} finally {
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			loginPage			= PageFactory.initElements(driver, LoginPage.class);
			menuPage			= PageFactory.initElements(driver, MenuPage.class);
			operadorPage		= PageFactory.initElements(driver, OperadorPage.class);
			zonaPage			= PageFactory.initElements(driver, ZonaPage.class);
			rutaPage			= PageFactory.initElements(driver, RutaPage.class);
			agendaPage			= PageFactory.initElements(driver, AgendaPage.class);
			sucursalesPage		= PageFactory.initElements(driver, SucursalesPage.class);
			tarifaPage			= PageFactory.initElements(driver, TarifaPage.class);
			serviciosPage		= PageFactory.initElements(driver, ServiciosPage.class);
		}
		return driver;
	}

	public static String getUserCL() {
		ReadConfigFile file = new ReadConfigFile();
		String userCL = file.getUserCL();
		return userCL;
	}
	
	public static String getPassCL() {
		ReadConfigFile file = new ReadConfigFile();
		String passCL = file.getPassCL();
		return passCL;
	}
	
	public static String getUserPE() {
		ReadConfigFile file = new ReadConfigFile();
		String userCL = file.getUserPE();
		return userCL;
	}
	
	public static String getPassPE() {
		ReadConfigFile file = new ReadConfigFile();
		String passCL = file.getPassPE();
		return passCL;
	}
	
	public static String getUserFail() {
		ReadConfigFile file = new ReadConfigFile();
		String userFail = file.getUserFail();
		return userFail;
	}
	
	public static String getPassFail() {
		ReadConfigFile file = new ReadConfigFile();
		String passFail = file.getPassFail();
		return passFail;
	}
}
